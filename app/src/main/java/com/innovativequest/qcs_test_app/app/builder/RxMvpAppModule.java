package com.innovativequest.qcs_test_app.app.builder;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ghous on 04/10/2019.
 */
@Module
public class RxMvpAppModule {

    private final Context context;

    public RxMvpAppModule(Context context) {
        this.context = context;
    }

    @Provides
    @AppScope
    public Context context() {
        return context;
    }

}
