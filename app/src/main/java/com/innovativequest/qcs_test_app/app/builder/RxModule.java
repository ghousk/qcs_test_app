package com.innovativequest.qcs_test_app.app.builder;

import com.twistedequations.rx2.AndroidRxSchedulers;
import com.twistedequations.rx2.DefaultAndroidRxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {

    @AppScope
    @Provides
    public AndroidRxSchedulers rxSchedulers() {
        return new DefaultAndroidRxSchedulers();
    }
}
