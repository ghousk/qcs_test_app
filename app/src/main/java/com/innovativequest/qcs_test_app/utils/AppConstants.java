package com.innovativequest.qcs_test_app.utils;

/**
 * Created by Ghous on 04/10/2019.
 */
public class AppConstants {

    private static final String TAG = "AppConstants";


    // ENV DEPENDENT BASE URLs
    public static final String BASE_URL_DEV = "https://api.github.com/repos/JetBrains/kotlin/";
    private static final String DEFAULT_LOCALE = "en-GB";

    public static String getLocale()
    {
        return DEFAULT_LOCALE;
    }

    public static final String GET_POSTS = "commits";
    public static final String GET_USERS = "resource/f9bf-2cp4.json";
    public static final String GET_COMMENTS = "comments";

}

