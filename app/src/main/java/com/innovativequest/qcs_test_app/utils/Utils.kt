package com.innovativequest.qcs_test_app.utils

import java.text.SimpleDateFormat

/**
 * Created by Ghous Khan on 04/10/2019
 */
object Utils {

    private val INPUT_DATE_FORMAT_FULL = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    private val OUTPUT_DATE_FORMAT = SimpleDateFormat("dd MMMM yyyy")


    fun getDateInDisplayableFormat(inputDateStr: String?): String{
        return OUTPUT_DATE_FORMAT.format(INPUT_DATE_FORMAT_FULL.parse(inputDateStr))
    }

}