package com.innovativequest.qcs_test_app.screens.home.mvp

import com.innovativequest.qcs_test_app.models.ItemDataResponse
import com.innovativequest.qcs_test_app.networkservices.DataService
import com.innovativequest.qcs_test_app.screens.home.HomeScreenActivity
import com.innovativequest.qcs_test_app.screens.itemdetail.ItemDetailActivity
import com.innovativequest.qcs_test_app.utils.PreferencesManager
import io.reactivex.Observable
import kotlin.collections.ArrayList

/**
 * Created by Ghous on 04/10/2019.
 */
class HomeScreenModel(private val mDataService: DataService, private val mHomeScreenActivity: HomeScreenActivity,
                      private val mPreferencesManager: PreferencesManager) {

    fun getItems(): Observable<ArrayList<ItemDataResponse>> {
       return mDataService.dataItems
    }

    fun showItemDetailScreen(itemItem: ItemDataResponse){
        ItemDetailActivity.start(mHomeScreenActivity, itemItem)
    }

    fun deleteData(){
        mPreferencesManager.clear()
        //Delete DB data here if required
        // Delete Files and Folder here if required
    }

}


