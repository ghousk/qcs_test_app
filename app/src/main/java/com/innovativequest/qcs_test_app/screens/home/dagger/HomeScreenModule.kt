package com.innovativequest.qcs_test_app.screens.home.dagger

import com.innovativequest.qcs_test_app.networkservices.DataService
import com.innovativequest.qcs_test_app.screens.home.HomeScreenActivity
import com.innovativequest.qcs_test_app.screens.home.mvp.DefaultHomeScreenView
import com.innovativequest.qcs_test_app.screens.home.mvp.HomeScreenModel
import com.innovativequest.qcs_test_app.screens.home.mvp.HomeScreenPresenter
import com.innovativequest.qcs_test_app.screens.home.mvp.HomeScreenView
import com.innovativequest.qcs_test_app.utils.PreferencesManager
import com.squareup.picasso.Picasso

import dagger.Module
import dagger.Provides

/**
 * Created by Ghous on 04/10/2019.
 */
@Module
class HomeScreenModule(internal val homeScreenActivity: HomeScreenActivity) {

    @Provides
    @HomeScreenScope
    fun homeScreenView(preferencesManager: PreferencesManager, picasso: Picasso): HomeScreenView {
        return DefaultHomeScreenView(homeScreenActivity, preferencesManager, picasso)
    }

    @Provides
    @HomeScreenScope
    fun homeScreenPresenter(homeScreenView: HomeScreenView,
                            homeScreenModel: HomeScreenModel): HomeScreenPresenter {
        return HomeScreenPresenter(homeScreenView, homeScreenModel)
    }

    @Provides
    @HomeScreenScope
    fun homeScreenModel(dataService: DataService, preferencesManager: PreferencesManager): HomeScreenModel {
        return HomeScreenModel(dataService, homeScreenActivity,  preferencesManager)
    }


}
