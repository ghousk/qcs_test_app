package com.innovativequest.qcs_test_app.screens.itemdetail.mvp

import com.innovativequest.qcs_test_app.models.ItemDataResponse
import com.innovativequest.qcs_test_app.models.ItemDetailSecond
import com.innovativequest.qcs_test_app.models.ItemDetailFirst
import com.innovativequest.qcs_test_app.networkservices.DataService
import io.reactivex.Observable
import kotlin.collections.ArrayList

/**
 * Created by Ghous on 04/10/2019.
 */
class ItemDetailModel(private val mDataService: DataService) {

    fun getItemDetailFirst() : Observable<ArrayList<ItemDetailFirst>> {
        return mDataService.itemDetailFirst
    }

//    fun getItemDetailSecond() : Observable<ArrayList<ItemDetailSecond>>{
//        return mDataService.itemDetailSecond
//    }
}


