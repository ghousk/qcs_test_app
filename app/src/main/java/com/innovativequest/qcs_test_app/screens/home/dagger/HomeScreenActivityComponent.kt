package com.innovativequest.qcs_test_app.screens.home.dagger

import com.innovativequest.qcs_test_app.app.builder.RxMvpAppComponent
import com.innovativequest.qcs_test_app.screens.home.HomeScreenActivity
import dagger.Component

/**
 * Created by Ghous on 04/10/2019.
 */
@HomeScreenScope
@Component(modules = arrayOf(HomeScreenModule::class), dependencies = arrayOf(RxMvpAppComponent::class))
interface HomeScreenActivityComponent {
    fun inject(homeScreenActivity: HomeScreenActivity)
}