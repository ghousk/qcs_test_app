package com.innovativequest.qcs_test_app.screens.home.mvp
import android.view.View
import com.innovativequest.qcs_test_app.models.ItemDataResponse
import io.reactivex.Observable

/**
 * Created by Ghous on 04/10/2019.
 */
interface HomeScreenView {
    val view: View

    fun showError(message: String)

    fun setLoading(loading: Boolean)

    fun hideKeyBoard()

    fun setListItems(itemList: List<ItemDataResponse>?)

    fun listItemClicks(): Observable<ItemDataResponse>

}