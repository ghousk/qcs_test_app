package com.innovativequest.qcs_test_app.screens.itemdetail.dagger

import com.innovativequest.qcs_test_app.networkservices.DataService
import com.innovativequest.qcs_test_app.screens.itemdetail.ItemDetailActivity
import com.innovativequest.qcs_test_app.screens.itemdetail.mvp.DefaultItemDetailView
import com.innovativequest.qcs_test_app.screens.itemdetail.mvp.ItemDetailModel
import com.innovativequest.qcs_test_app.screens.itemdetail.mvp.ItemDetailPresenter
import com.innovativequest.qcs_test_app.screens.itemdetail.mvp.ItemDetailView
import com.innovativequest.qcs_test_app.utils.PreferencesManager
import com.squareup.picasso.Picasso

import dagger.Module
import dagger.Provides

/**
 * Created by Ghous on 04/10/2019.
 */
@Module
class ItemDetailModule(internal val itemDetailActivity: ItemDetailActivity) {

    @Provides
    @ItemDetailScope
    fun itemDetailView(picasso: Picasso): ItemDetailView {
        return DefaultItemDetailView(itemDetailActivity, picasso)
    }

    @Provides
    @ItemDetailScope
    fun itemDetailPresenter(itemDetailView: ItemDetailView,
                            itemDetailModel: ItemDetailModel,
                            preferencesManager: PreferencesManager): ItemDetailPresenter {
        return ItemDetailPresenter(itemDetailActivity, itemDetailView, itemDetailModel, preferencesManager)
    }

    @Provides
    @ItemDetailScope
    fun itemDetailModel(dataService: DataService): ItemDetailModel {
        return ItemDetailModel(dataService)
    }


}
