package com.innovativequest.qcs_test_app.screens.itemdetail.mvp

import android.util.Log
import com.innovativequest.qcs_test_app.models.ItemDataResponse
import com.innovativequest.qcs_test_app.screens.itemdetail.ItemDetailActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.innovativequest.qcs_test_app.utils.PreferencesManager
import io.reactivex.android.schedulers.AndroidSchedulers


/**
 * Created by Ghous on 04/10/2019.
 */
class ItemDetailPresenter (private val activityItem: ItemDetailActivity, private val itemDetailView: ItemDetailView, private  val itemDetailModel: ItemDetailModel,
                           private val preferencesManager: PreferencesManager) {

    private val compositeDisposable = CompositeDisposable()

    fun onCreate(dataItem: ItemDataResponse) {

        compositeDisposable.addAll(
                subscribeToBackButton())

        itemDetailView.setItem(dataItem)
    }

    private fun subscribeToBackButton(): Disposable {
        return itemDetailView.toolbarStartBtnObs().subscribe {
            activityItem.onBackPressed()
        }
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

}