package com.innovativequest.qcs_test_app.screens.itemdetail.mvp
import android.view.View
import com.innovativequest.qcs_test_app.models.ItemDataResponse
import io.reactivex.Observable

/**
 * Created by Ghous on 04/10/2019.
 */
interface ItemDetailView {
    val view: View

    fun showError(message: String)

    fun setItem(dataItem: ItemDataResponse)

    fun toolbarStartBtnObs(): Observable<Any>
}