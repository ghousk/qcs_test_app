package com.innovativequest.qcs_test_app.screens.itemdetail.dagger

import com.innovativequest.qcs_test_app.app.builder.RxMvpAppComponent
import com.innovativequest.qcs_test_app.screens.itemdetail.ItemDetailActivity
import dagger.Component

/**
 * Created by Ghous on 04/10/2019.
 */
@ItemDetailScope
@Component(modules = arrayOf(ItemDetailModule::class), dependencies = arrayOf(RxMvpAppComponent::class))
interface ItemDetailActivityComponent {
    fun inject(itemDetailActivity: ItemDetailActivity)
}