package com.innovativequest.qcs_test_app.models

import com.google.gson.annotations.SerializedName

data class Tree(

	@field:SerializedName("sha")
	val sha: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)