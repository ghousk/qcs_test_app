package com.innovativequest.qcs_test_app.models

import com.google.gson.annotations.SerializedName

data class ParentsItem(

	@field:SerializedName("html_url")
	val htmlUrl: String? = null,

	@field:SerializedName("sha")
	val sha: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)