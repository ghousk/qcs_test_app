package com.innovativequest.qcs_test_app.models

import com.google.gson.annotations.SerializedName

data class Commit(

	@field:SerializedName("comment_count")
	val commentCount: Int? = null,

	@field:SerializedName("committer")
	val committer: Committer? = null,

	@field:SerializedName("author")
	val author: Author? = null,

	@field:SerializedName("tree")
	val tree: Tree? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("verification")
	val verification: Verification? = null
)